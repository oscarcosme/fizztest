function overflowHiddenOn() {
	$('html, body').css('overflow', 'hidden');
	//disableScroll();
}
function overflowHiddenOff() {
	$('html, body').css('overflow', 'auto');
	//enableScroll();
}
function esMobile(){
	return ($(window).width()<768)?true:false;
}
function jsScrollTo(elemento){
	$('html, body').animate({
        scrollTop: $(elemento).offset().top
    }, 1000);
}


$(function(){
	g_Preloader.active(true);
	$(document).ajaxStart(function () {
		g_Preloader.active(true);
	}).ajaxStop(function () {
		g_Preloader.active(false);
	});

	$('body').flowtype({
		minimum   : 500,
		maximum   : 1200,
		minFont   : 12,
		maxFont   : 18,
		fontRatio : 40
	});

	$('.boxSlider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		focusOnSelect: true,
		arrows: false,
		dots: true,
		// adaptiveHeight: true,
		responsive: [{
			breakpoint: 415,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true
			}
		}]
	});

	// SUBMENU
	$('.navigation ul.menu li:first-child').mouseover(function(){
		$('.navigation .submenu').fadeIn('slow');
	});
	$('.navigation .submenu').mouseleave(function(){
		$('.navigation .submenu').fadeOut('slow');
	});


	// FILTROS - SIDEBAR
	$('.sidebar > ul > li').on('click', function(){
		$('.sidebar ul li').removeClass('active');
		$('.sidebar ul li ul').removeClass('show');

		var puntero = $(this);
		puntero.addClass('active');

		if(puntero.hasClass('active')){
			// puntero.find("ul").addClass('show');
			$('.sidebar ul li.active ul').addClass('show');
		}
	});

	//LLAMADAS AJAX
	$('.brands ul li img').on('click', function(){
		$.ajax({
			url: 'http://remote.fizzmod.com/ajax.php',
			type: 'POST',
			success : function(json) {
				$('.brands ul').fadeOut('slow');

				$('<h1/>').text(json.title).appendTo('.brands');
				// $('<div class="content"/>').html(json.html).appendTo('body');
			},
			error : function(xhr, status) {
				alert('Disculpe, existió un problema');
			},
		});

	});

	// HOVER IMG
	$('.products .image').mouseover(function(){
		var puntero = $(this).parent();
		
		puntero.find('.price ul li:first-child').css({'color':'green', 'font-size':'17px'});
		puntero.find('h4').css('transform','scale(1.05)');
	});
	$('.products .image').mouseout(function(){
		var puntero = $(this).parent();
		puntero.find('.price ul li:first-child').css('color','#D44700');
		puntero.find('h4').css('transform','scale(1)');
	});


	



		







































});



$(window).on('load', function (e) {
	setTimeout(function(){
		g_Preloader.active(false);
	}, 1000);
});
