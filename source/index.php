<?php include 'fixed/header.php'; ?>
<?php include 'fixed/menu.php'; ?>

<header>
	<div class="container">
		<div class="left logo">
			<h1>fizz<span>test</span></h1>
		</div>
		<div class="right">
			<div class="top">
				<ul>
					<li><a href=""><i class="fa fa-thumb-tack" aria-hidden="true"></i> Sucursales</a></li>
					<li><a href=""><i class="fa fa-cart-plus" aria-hidden="true"></i> Como comprar</a></li>
					<li>|</li>
					<li><a href="">Mis pedidos</a></li>
				</ul>
			</div>
			<div class="bottom">
				<div class="search">
					<input type="text" placeholder="Buscar por productos, deportes o marcas">
					<a><i class="fa fa-search" aria-hidden="true"></i></a>
				</div>
				<div class="ads">
					<img src="assets/img/adsFutbol.jpg" alt="" />
				</div>
			</div>
		</div>
	</div>
</header>
<nav>
	<div class="container">
		<div class="navigation">
			<ul class="menu">
				<li><a>Hombre</a></li>
				<li><a>Mujer</a></li>
				<li><a>Niños</a></li>
			</ul>
			<div class="submenu">
				<div class="contenedor">
					<div class="arrow-up"></div>
					<div class="box left">
						<span>Calzado</span>
						<ul>
							<li><a>Botines</a></li>
							<li><a>Zapatillas</a></li>
							<li><a>Botines con tapones</a></li>
							<li><a>Ojotas / Chinelas</a></li>
							<li><a>Sandalia</a></li>
							<li><a>Ver más</a> <i class="fa fa-angle-right" aria-hidden="true"></i></li>
						</ul>
					</div>
					<div class="box right">
						<div class="thumbnail">
							<img src="assets/img/medias.png" alt="" />
						</div>
						<div class="excerpt">
							<h4>Medias adidas adiliner HC 3S</h4>
							<div class="price">
								<ul>
									<li>$109,00</li>
									<li class="brand"><img src="assets/img/adidas.jpg" alt="" /></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="carrito">
			<ul>
				<li>Carrito</li>
				<li><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>0</span></li>
				<li>$ 0,00</li>
			</ul>
		</div>
	</div>
</nav>

<section>
	<div class="container">
		<div class="boxSlider">
			<div><img src="assets/img/banner01.jpg" alt="" /></div>
			<div><img src="http://via.placeholder.com/980x400"></div>
		</div>
		<div class="brands">
			<ul>
				<li><img src="assets/img/adidas.jpg" alt=""></li>
				<li><img src="assets/img/nike.jpg" alt=""></li>
				<li><img src="assets/img/adidas2.jpg" alt=""></li>
				<li><img src="assets/img/neo.jpg" alt=""></li>
				<li><img src="assets/img/puma.jpg" alt=""></li>
				<li><img src="assets/img/topper.jpg" alt=""></li>
				<li><img src="assets/img/default.jpg" alt=""></li>
				<li><img src="assets/img/converse.jpg" alt=""></li>
				<li><img src="assets/img/salomon.jpg" alt=""></li>
				<li><img src="assets/img/disney.jpg" alt=""></li>
			</ul>
		</div>
	</div>

	<div class="wrapper">
		<div class="container">
			<div class="box sidebar">
				<span class="filtro">Filtro por</span>
				<ul>
					<li class="">
						<span>Sexo
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</span>
						<ul>
							<li><input type="checkbox">Hombre</li>
							<li><input type="checkbox">Mujer</li>
							<li><input type="checkbox">Niños</li>
						</ul>
					</li>
					<li>
						<span>Deporte
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</span>
						<ul>
							<li><input type="checkbox">Fútbol</li>
							<li><input type="checkbox">Rugby</li>
							<li><input type="checkbox">Tennis</li>
						</ul>
					</li>
					<li>
						<span>Color
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						</span>
						<ul>
							<li><input type="checkbox">Azul</li>
							<li><input type="checkbox">Verde</li>
							<li><input type="checkbox">Rojo</li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="box products">
				<ul>
					<li>
						<div class="image">
							<img src="assets/img/product_01.jpg" alt="" />
						</div>
						<div class="excerpt">
							<h4>Camiseta de futbol Adidas River Plate Away</h4>
							<div class="price">
								<ul>
									<li>$849.00</li>
									<li class="brand"><img src="assets/img/adidas.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="category">
								Camisetas futbol
							</div>
						</div>
					</li>
					<li>
						<div class="image">
							<img src="assets/img/product_02.jpg" alt="" />
						</div>
						<div class="excerpt">
							<h4>Ojotas adidas Duramo Thong</h4>
							<div class="price">
								<ul>
									<li>$299.00</li>
									<li class="brand"><img src="assets/img/adidas.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="category">
								Ojostas Chinelas
							</div>
						</div>
					</li>
					<li>
						<div class="image">
							<img src="assets/img/product_03.jpg" alt="" />
						</div>
						<div class="excerpt">
							<div class="oferta">Oferta</div>
							<h4>Zapatillas nike roubaix IIV</h4>
							<div class="price">
								<ul>
									<li>$450.00</li>
									<li class="brand"><img src="assets/img/nike.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="category">
								Zapatillas
							</div>
						</div>
					</li>
					<li>
						<div class="image">
							<img src="assets/img/product_04.jpg" alt="" />
						</div>
						<div class="excerpt">
							<h4>Camiseta de futbol Adidas River Plate Home</h4>
							<div class="price">
								<ul>
									<li>$849.00</li>
									<li class="brand"><img src="assets/img/adidas.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="category">
								Ojostas Chinelas
							</div>
						</div>
					</li>
					<li>
						<div class="image">
							<img src="assets/img/product_05.jpg" alt="" />
						</div>
						<div class="excerpt">
							<h4>Pantalon Jockey dama</h4>
							<div class="price">
								<ul>
									<li>$150.00</li>
									<li class="brand"><img src="assets/img/adidas.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="category">
								Pantalones
							</div>
						</div>
					</li>
					<li>
						<div class="image">
							<img src="assets/img/product_06.jpg" alt="" />
						</div>
						<div class="excerpt">
							<div class="oferta">Oferta</div>
							<h4>Camiseta de futbol Nike Boca home Authentic</h4>
							<div class="price">
								<ul>
									<li>$399.00</li>
									<li class="brand"><img src="assets/img/nike.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="category">
								Camisetas futbol
							</div>
						</div>
					</li>
					<li>
						<div class="image">
							<img src="assets/img/product_07.jpg" alt="" />
						</div>
						<div class="excerpt">
							<h4>Short nike squad strike LGR</h4>
							<div class="price">
								<ul>
									<li>$399.00</li>
									<li class="brand"><img src="assets/img/nike.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="category">
								Short
							</div>
						</div>
					</li>
					<li>
						<div class="image">
							<img src="assets/img/product_08.jpg" alt="" />
						</div>
						<div class="excerpt">
							<div class="oferta">Oferta</div>
							<h4>Zapatillas adidas original schollstar cf k</h4>
							<div class="price">
								<ul>
									<li>$370.00</li>
									<li class="brand"><img src="assets/img/adidas2.jpg" alt="" /></li>
								</ul>
							</div>
							<div class="category">
								Zapatillas
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>





















<?php include 'fixed/footer.php'; ?>